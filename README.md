# Rustycat

Regex-based parser for Android Debug Brigde (adb) logcat.

## Format support

- [**Punt**](https://github.com/kashifrazzaqui/punt)
- [**threadtime**](https://developer.android.com/tools/logcat#outputFormat)

## Usage

### Command line
Rustycat can be used as cli tool. To do this please download pre-compiled binaries from repo or build with feature `build-binary`

Example:
```
rustycat threadtime -i logs.txt
```

### Library

Rustycat is foremost a library that you can use inside your application. To do this import it by adding it to your `Cargo.toml`

```toml
[dependencies]
rustycat = "0.1.0"
```

### Serde
Rustycat also supports serde via feature flag if you don't want this you can disable it by turing off `default-features`

```toml
[dependencies]
rustycat = { version = "0.1.0", default-features = false }
```

## License

Licensed under ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

## Contribution

Feel free to open an issue or fork the repo. I will check it in free time.