pub mod punt;
pub mod threadtime;
pub mod record;

pub trait Parser {
    /// Parses provided and outputs parsed `Record`
    ///
    /// # Notes
    /// - `Record` will always be generated even if it contains only `raw` field.
    /// - `Punt` and `Threadtime` implementations uses regex to split input line into parts,
    /// because of this if input isn't in expected format `Record` fields can have invalid/malformed data.
    ///
    /// # Example
    /// ```rust
    /// use rustycat::{Parser, ThreadtimeParser};
    ///
    /// // Loaded file content
    /// let data: String = "...".to_string();
    /// let parser = ThreadtimeParser::new();
    /// let records = data.lines()
    ///     .map(|line| parser.parse_line(line))
    ///     .collect::<Vec<_>>();
    /// println!("{:?}", records);
    /// ```
    fn parse_line(&self, line: &str) -> record::Record;
}