use crate::logcat::Parser;
use crate::logcat::record::{Level, Record};

/// Regex logcat punt format
///
/// Group #1: Data: YYYY-MM-DD or MM-DD
/// Group #2: Time: hh:mm:ss.fff
/// Group #3: Process ID
/// Group #4: Thread ID
/// Group #5: Log level
/// Group #6: Log number
/// Group #7: Tag
/// Group #8: Message
const LOG_FORMAT_REGEX: &str = r"(?P<date>\d{0,4}-{0,1}\d{2}-\d{2}){0,}\s*(?P<time>\d{2}:\d{2}:\d{2}.\d{0,3}){0,}\s*(?P<pid>\d+){0,}(?P<tid>\({1}\d+\){1}){0,}\s*(?P<level>\w){0,}\s*(?P<log_number>#\d+){0,}\s*(?P<tag>\w+){0,}\s*(?P<message>.+)\n{0,}";

/// `Parser` trait implementation for log lines from `punt`.
///  See [`punt` program](https://github.com/kashifrazzaqui/punt)
#[derive(Debug)]
pub struct PuntParser(regex::Regex);

impl PuntParser {
    pub fn new() -> Self {
        Self(regex::Regex::new(LOG_FORMAT_REGEX).expect("Invalid regex expression"))
    }
}

impl Default for PuntParser {
    fn default() -> Self {
        Self::new()
    }
}

impl Parser for PuntParser {
    fn parse_line(&self, line: &str) -> Record {
        let mut record = Record {
            raw: line.to_string(),
            ..Default::default()
        };
        if let Some(cap) = self.0.captures(line) {
            record.parse_date_time(
                cap.name("date").map_or("", |m| m.as_str()),
                cap.name("time").map_or("", |m| m.as_str()),
            );
            record.parse_process_id(cap.name("pid").map_or("", |m| m.as_str()));
            record.parse_thread_id(
                cap.name("tid")
                    .map_or("", |m| m.as_str().trim_matches(
                        |c: char| c.eq_ignore_ascii_case(&'(') || c.eq_ignore_ascii_case(&')')
                    )));
            record.level = Level::from(cap.name("level").map_or("", |m| m.as_str()));
            record.parse_log_number(cap.name("log_number").map_or("", |m| m.as_str().trim_matches('#')));
            record.tag = cap.name("tag").map_or("", |m| m.as_str()).to_string();
            record.message = cap.name("message").map_or("", |m| m.as_str()).to_string();
        }
        record
    }
}

