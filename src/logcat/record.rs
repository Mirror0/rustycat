use chrono::{NaiveDate, NaiveDateTime, NaiveTime};
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

/// Enum representing `'adb logcat'` priority level
#[derive(Clone, Debug, PartialOrd, Ord, PartialEq, Eq, Default)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub enum Level {
    Verbose = 0,
    Debug = 1,
    Info = 2,
    Warning = 3,
    Error = 4,
    Fatal = 5,
    Silent = 6,
    #[default]
    Unknown = -1,
}

impl From<&str> for Level {
    fn from(value: &str) -> Self {
        let first = value.to_uppercase()
            .chars()
            .rev()
            .last()
            .unwrap_or_default();
        match first {
            'V' => Level::Verbose,
            'D' => Level::Debug,
            'I' => Level::Info,
            'W' => Level::Warning,
            'E' => Level::Error,
            'F' => Level::Fatal,
            'S' => Level::Silent,
            _ => Level::Unknown
        }
    }
}

/// Struct that represent single line of `'adb logcat'` output.
/// It contains all parsed data as well as raw representation of it.
///
/// # Serde
/// While using with `serde` (de)serialization will skip `raw` field
#[derive(Clone, Debug, Default, PartialEq, Eq, Ord, PartialOrd)]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
pub struct Record {
    pub timestamp: Option<NaiveDateTime>,
    pub message: String,
    pub level: Level,
    pub tag: String,
    pub log_number: Option<i64>,
    pub process_id: Option<i32>,
    pub thread_id: Option<i32>,
    #[serde(skip)]
    pub raw: String,
}

impl Record {
    /// Parse date and time and write it to `timestamp` field
    ///
    /// # Format
    /// See [`format::strftime` module](crate::format::strftime)
    /// Date should be `%F` or `%m-%d`, which will be padded with year 1970.
    /// Time should be `%T.f`
    ///
    /// # Errors
    /// This method do not generate any error, but if either date or time fail to parse, the type `Default` will be used.
    /// If both values fail to parse a `None` will be written into `timestamp` field.
    pub fn parse_date_time(&mut self, date: &str, time: &str) {
        let date = NaiveDate::parse_from_str(date, "%F")
            .or_else(|_| NaiveDate::parse_from_str(&("1970-".to_string() + date), "%Y-%m-%d")).ok();
        let time = NaiveTime::parse_from_str(time, "%T%.f").ok();
        self.timestamp = match (date, time) {
            (Some(ok_date), Some(ok_time)) => {
                Some(NaiveDateTime::new(ok_date, ok_time))
            }
            (Some(ok_date), None) => {
                Some(NaiveDateTime::new(ok_date, NaiveTime::default()))
            }
            (None, Some(ok_time)) => {
                Some(NaiveDateTime::new(NaiveDate::default(), ok_time))
            }
            (None, None) => None
        }
    }

    /// Parse process id (decimal) and write it to `process_id` field
    ///
    /// # Errors
    /// This method do not generate any error, but if string parsing fails `None` is written.
    pub fn parse_process_id(&mut self, id: &str) {
        self.process_id = id.parse::<i32>().ok()
    }

    /// Parse thread id (decimal) and write it to `thread_id` field
    ///
    /// # Errors
    /// This method do not generate any error, but if string parsing fails `None` is written.
    pub fn parse_thread_id(&mut self, id: &str) {
        self.thread_id = id.parse::<i32>().ok()
    }

    /// Parse log number (decimal) and write it to `log_number` field
    ///
    /// # Errors
    /// This method do not generate any error, but if string parsing fails `None` is written.
    pub fn parse_log_number(&mut self, num: &str) {
        self.log_number = num.parse::<i64>().ok()
    }
}