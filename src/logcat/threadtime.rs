use crate::logcat::Parser;
use crate::logcat::record::{Level, Record};

/// Regex logcat threadtime format (Default format)
///
/// Group #1: Data: YYYY-MM-DD or MM-DD
/// Group #2: Time: hh:mm:ss.fff
/// Group #3: Process ID
/// Group #4: Thread ID
/// Group #5: Log level
/// Group #6: Tag
/// Group #7: Message
const LOG_FORMAT_REGEX: &str = r"(?P<date>\d{0,4}-{0,1}\d{2}-\d{2}){0,}\s*(?P<time>\d{2}:\d{2}:\d{2}.\d{0,3}){0,}\s*(?P<pid>\d+){0,}\s*(?P<tid>\w+){0,}\s*(?P<level>\w){0,}\s*(?P<tag>\w+:){0,}\s*(?P<message>.+)\n{0,}";

/// `Parser` trait implementation for log lines from `'adb logcat'` default output.
///  See [`threadtime` format](https://developer.android.com/tools/logcat#outputFormat)
#[derive(Debug)]
pub struct ThreadtimeParser(regex::Regex);

impl ThreadtimeParser {
    pub fn new() -> Self {
        Self(regex::Regex::new(LOG_FORMAT_REGEX).expect("Invalid regex expression"))
    }
}

impl Default for ThreadtimeParser {
    fn default() -> Self {
        Self::new()
    }
}

impl Parser for ThreadtimeParser {
    fn parse_line(&self, line: &str) -> Record {
        let mut record = Record {
            raw: line.to_string(),
            ..Default::default()
        };
        if let Some(cap) = self.0.captures(line) {
            record.parse_date_time(
                cap.name("date").map(|m| m.as_str()).unwrap_or_default(),
                cap.name("time").map(|m| m.as_str()).unwrap_or_default(),
            );
            record.parse_process_id(cap.name("pid").map_or("", |m| m.as_str()));
            record.parse_thread_id(cap.name("tid").map_or("", |m| m.as_str()));
            record.level = Level::from(cap.name("level").map_or("", |m| m.as_str()));
            record.tag = cap.name("tag").map_or("", |m| m.as_str().trim_matches(':')).to_string();
            record.message = cap.name("message").map_or("", |m| m.as_str()).to_string();
        }
        record
    }
}