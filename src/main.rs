use std::fmt::Display;
use std::io::Write;
use std::path::PathBuf;
use std::str::FromStr;
use crate::logcat::Parser;
use crate::logcat::punt::PuntParser;
use crate::logcat::threadtime::ThreadtimeParser;
use clap::{Command, Arg, value_parser, ArgMatches, Parser as ClapParser, Subcommand, Args as ClapArgs, ValueEnum};

pub(crate) mod logcat;

#[derive(ClapParser)]
#[command(version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Parse file using `punt` format
    Punt(Args),
    /// Parse file using default logcat `threadtime` format
    Threadtime(Args),
}


#[derive(ClapArgs)]
struct Args {
    /// Path to input file
    #[arg(
    short,
    long,
    value_name = "FILE"
    )]
    input: PathBuf,

    #[arg(
    short,
    long,
    value_name = "FILE",
    help = "Path to output JSON file, creates file if not exists, truncate if exists",
    value_parser = value_parser!(PathBuf)
    )]
    output: Option<PathBuf>,
}

#[derive(Debug)]
pub struct RuntimeError(String);

fn main() {
    let cli = Cli::parse();
    let result = match cli.command {
        Commands::Punt(args) => execute(Box::new(PuntParser::new()), args),
        Commands::Threadtime(args) => execute(Box::new(ThreadtimeParser::new()), args)
    };
    if let Err(e) = result {
        eprintln!("Error: {:?}", e);
    }
}

fn execute(parser: Box<dyn Parser>, args: Args) -> Result<(), RuntimeError> {
    let records = std::fs::read_to_string(args.input)
        .map_err(|e| RuntimeError(e.to_string()))?
        .lines()
        .map(|s| parser.parse_line(s))
        .collect::<Vec<_>>();
    if let Some(out) = args.output {
        let mut opts = std::fs::OpenOptions::new();
        opts.write(true);
        opts.create(true);
        opts.truncate(true);
        let mut file = opts.open(out).map_err(|e| RuntimeError(e.to_string()))?;
        file.write(
            &serde_json::to_vec_pretty(&records)
                .map_err(|e| RuntimeError(e.to_string()))?
        ).map_err(|e| RuntimeError(e.to_string()))?;
    } else {
        println!("{}",
                 serde_json::to_string_pretty(&records)
                     .map_err(|e| RuntimeError(e.to_string()))?
        );
    }
    Ok(())
}