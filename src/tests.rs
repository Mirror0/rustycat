use crate::record::{Level, Record};
use pretty_assertions::assert_eq;
use crate::{Parser, PuntParser, ThreadtimeParser};

const PUNT_LOGS: &str = include_str!("../test_res/punt.txt");
const TT_LOGS: &str = include_str!("../test_res/threadtime.txt");

#[test]
fn test_record_parse_date_time_full_year() {
    let mut record = Record::default();
    record.parse_date_time("2024-03-29", "12:00:00.001");
    assert!(record.timestamp.is_some());
    let date = record.timestamp.unwrap();
    assert_eq!("2024-03-29 12:00:00.001", format!("{}", date));
}

#[test]
fn test_record_parse_date_time_no_year() {
    let mut record = Record::default();
    record.parse_date_time("03-29", "12:00:00.001");
    assert!(record.timestamp.is_some());
    let date = record.timestamp.unwrap();
    assert_eq!("1970-03-29 12:00:00.001", format!("{}", date));
}

#[test]
fn test_record_parse_date_time_no_time() {
    let mut record = Record::default();
    record.parse_date_time("03-29", "");
    assert!(record.timestamp.is_some());
    let date = record.timestamp.unwrap();
    assert_eq!("1970-03-29 00:00:00", format!("{}", date));
}

#[test]
fn test_record_parse_date_time_no_date() {
    let mut record = Record::default();
    record.parse_date_time("", "12:00:00.001");
    assert!(record.timestamp.is_some());
    let date = record.timestamp.unwrap();
    assert_eq!("1970-01-01 12:00:00.001", format!("{}", date));
}

#[test]
fn test_parse_punt() {
    let p = PuntParser::new();
    let expected = prepare_punt_records();
    let res = PUNT_LOGS.lines().map(|l| p.parse_line(l)).collect::<Vec<_>>();
    assert_eq!(expected, res);
}

#[test]
fn test_parse_threadtime() {
    let p = ThreadtimeParser::new();
    let expected = prepare_threadtime_records();
    let res = TT_LOGS.lines().map(|l| p.parse_line(l)).collect::<Vec<_>>();
    assert_eq!(expected, res);
}

fn prepare_punt_records() -> Vec<Record> {
    let mut r1 = Record::default();
    let mut r2 = Record::default();
    r1.parse_date_time("03-28", "21:11:40.500");
    r1.process_id = Some(1854);
    r1.thread_id = Some(1854);
    r1.level = Level::Debug;
    r1.log_number = Some(2);
    r1.tag = "ServiceImpl".to_string();
    r1.message = "onSensorChanged type = 2, value = 0.0".to_string();
    r1.raw = "03-28 21:11:40.500 1854(1854) D #2 ServiceImpl  onSensorChanged type = 2, value = 0.0".to_string();
    r2.timestamp = r1.timestamp.clone();
    r2.process_id = Some(1854);
    r2.thread_id = Some(1854);
    r2.level = Level::Debug;
    r2.log_number = Some(3);
    r2.tag = "ServiceImpl".to_string();
    r2.message = "setValue to 803".to_string();
    r2.raw = "03-28 21:11:40.500 1854(1854) D #3 ServiceImpl  setValue to 803".to_string();
    vec![r1, r2]
}

fn prepare_threadtime_records() -> Vec<Record> {
    let mut r1 = Record::default();
    let mut r2 = Record::default();
    r1.parse_date_time("03-25", "17:35:48.297");
    r1.process_id = Some(1648);
    r1.thread_id = Some(1648);
    r1.level = Level::Debug;
    r1.tag = "ServiceImpl".to_string();
    r1.message = "onSensorChanged type = 2, value = 0.0".to_string();
    r1.raw = "03-25 17:35:48.297  1648  1648 D ServiceImpl: onSensorChanged type = 2, value = 0.0".to_string();
    r2.timestamp = r1.timestamp.clone();
    r2.process_id = Some(1648);
    r2.thread_id = Some(1648);
    r2.level = Level::Debug;
    r2.tag = "ServiceImpl".to_string();
    r2.message = "setValue to 803".to_string();
    r2.raw = "03-25 17:35:48.297  1648  1648 D ServiceImpl: setValue to 803".to_string();
    vec![r1, r2]
}